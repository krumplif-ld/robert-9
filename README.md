
<h1>Robertdot9</h1>

This discord bot was created in order to prevent spam and gif spams in community servers.

Currently it works by getting 20 messages from the channel, and checking the current message against those, and then if the exact
same message or a similar one (length check for less than 21% difference) is found multiple times it deletes it.

The gif spam prevention works in a similar way, 2 gifs are allowed per 20 messages. Checks for both uploaded gifs and link embeded gifs.
This is done in order so that people would rather add to an ongoing conversation than disturb it with gifs.
