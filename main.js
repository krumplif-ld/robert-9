// const { channel } = require('diagnostic_channel')
const {
  error,
  debug
} = require('console')
const Discord = require('discord.js')
const {
  get
} = require('http')
const client = new Discord.Client()
const {
  token
} = require('./config.json')
const asyncHooks = require('async_hooks')
const {
  Channel
} = require('diagnostics_channel')

const DOLGOZGATO_KRMPL_ID = '509077409077788672'
const FOKRUMPLI_ID = '509075485104406536'
const JERRYYHALI = ':jerryyHali:764173202191679508'
const KONCHA = ':koncha:861314424518606909'
const HALINONSUB = ':hali:930763591261495317';
const maxChatspam = 3
const maxGifspam = 3
const maxSameTorelance = 0.2
const KRMPL_FOLD_ID = '509075162826539028';
const ROBOT_TESTING_ID = '851541624852185089';
const UNDER_SURV = KRMPL_FOLD_ID;

var channel;
var messagesHistory = new Array();
var limiter = 10;
var messagesHistoryGif = new Array();
var limiterGif = 20;


client.on('ready', () => {
  channel = client.channels.cache.get(UNDER_SURV);
  channel.messages.fetch({
      limit: limiter
    })
    .then(messages => {
      messages.forEach(message => {
        messagesHistory.push(message)
      });
    });
  messagesHistory = messagesHistory.reverse();

  channel.messages.fetch({
      limit: limiterGif
    })
    .then(messages => {
      messages.forEach(message => {
        messagesHistoryGif.push(message)
      });
    });
  messagesHistoryGif = messagesHistoryGif.reverse();
  console.log(`Logged in as ${client.user.tag}!`)
})

// AGYAD CMD

client.on('message', msg => {
  if (msg.content === '$agyad?') {
    msg.reply('0%')
    if (msg.member.roles.cache.has(DOLGOZGATO_KRMPL_ID) ||
      msg.member.roles.cache.has(FOKRUMPLI_ID)) {
      client.destroy()
    }
  }
})

// This checks the last $limiter amount of messages, (done)
// if it finds more then 1,                         (done)
// deletes message                                  (done)
// Also bunch of console logs, which slow down everything comment or delete them later (lol)
// Add hali Emoji as exception(done), add roles to exception(done) 
// switch if ordering, so it only runs if necessary(done?)
// more logs trollLaugh (done, add more if needed)
// Rethink the 0.2 stuff (testing)
// Make an emoji detector (regex?) so emojis will be 1 character len, instead of 20+x (TODO)
//bug in gifchecker(very rare case) (TODO)
//add db reinit if msg is deleted by not the bot(TODO)
// check if msg has a gif, so only one client.on runs (testing)

// spam check
client.on('message', msg => {

  if (msg.channel.id === UNDER_SURV) {

    messageHistoryManagerGif("add", msg); //add to gifdb
    messageHistoryManager("add", msg); //add new message to the messagesHistory

    if (IsMessageALinkGif(msg.content) //msg is a link gif
      ||
      (msg.attachments.size >= 1) //msg is an uploaded attachment
      || 
      msg.member.roles.cache.has(DOLGOZGATO_KRMPL_ID) //msg was sent by moderator
      ||
      msg.member.roles.cache.has(FOKRUMPLI_ID) //msg was sent by admin
      ||
      msg.content.match(JERRYYHALI) // it's a single hali
      ||
      msg.content.match(KONCHA) // it's a single koncha
      ||
      msg.content.match(HALINONSUB)
      ||
      msg.author.id === client.id) //it was sent by the bot
    {
      return;
    }
    var counter = 0
    var longer = new Array(); //where was the message caught(was it equal, shorter or longer?)
    let outScopeOldContent = new Array();
    let outScopeNewContent;
    let newContent = msg.content;

    messagesHistory.forEach(message => {
      if (message.content === undefined) {
        console.log(message);
      }
      let oldContent = message.content;

      if (oldContent.toLowerCase() === newContent.toLowerCase() && oldContent != '') {
        counter += 1;
        longer.push(0);
        outScopeOldContent.push(oldContent);
      } else if (oldContent.length > newContent.length && oldContent != '') { //old longer then new
        if (oldContent.toLowerCase().includes(newContent.toLowerCase())) { //old has new message inside it
          if (newContent.length / oldContent.length >= maxSameTorelance) {
            counter += 1;
            longer.push(1);
            outScopeOldContent.push(oldContent);
          }
        }
      } else if (oldContent.content != '') { //new longer then old
        if (newContent.toLowerCase().includes(oldContent.toLowerCase())) {
          if (oldContent.length / newContent.length >= maxSameTorelance) {
            counter += 1;
            longer.push(2);
            outScopeOldContent.push(oldContent);
          }
        }
      }
      outScopeNewContent = newContent;
    })

    if (counter >= maxChatspam) {
      messageHistoryManager("rm", msg);
      messageHistoryManagerGif("rm", msg);
      spamLogger(msg.author.username, outScopeOldContent, outScopeNewContent, longer);
      deleteMessage(msg);
    }
  }
});

// gif check
client.on('message', msg => {

  if (msg.channel.id === UNDER_SURV) {
    var currentgifName = new Array();
    let currentLinkGifs = linkGifHandler(msg, currentgifName);
    let currentUploadGifs = uploadedGifHandler(msg, currentgifName);
    let currentgifAmount = currentLinkGifs + currentUploadGifs;

    if (currentgifAmount <= 0) { //run only if it's either a link or has attachment
      return;
    }

    newGifHandler(msg, currentLinkGifs, currentUploadGifs, currentgifName);
  }
});

function newGifHandler(msg, currentLinkGifs, currentUploadGifs, currentgifName) {

  let amountOfGifs = 0;
  let msgLinkOrUpload = 0; //1 upload, 2 link, 3 both, 0 error 
  let oldGifAmount = 0;

  if(currentLinkGifs > 0 && currentUploadGifs <= 0) msgLinkOrUpload = 1;
  else if(currentLinkGifs <= 0 && currentUploadGifs > 0) msgLinkOrUpload = 2;
  else if(currentLinkGifs > 0 && currentUploadGifs > 0) msgLinkOrUpload = 3;
  else msgLinkOrUpload = 0;

  messagesHistoryGif.forEach(message => { //get amount of gifs in history
    amountOfGifs += uploadedGifHandler(message);
    amountOfGifs += linkGifHandler(message);
    oldGifAmount = amountOfGifs;
  });

  gifLogger("inc", msg.author.username, msg, msgLinkOrUpload, currentgifName, amountOfGifs);

  if (amountOfGifs >= maxGifspam) {
    gifLogger("rm", msg.author.username, msg, msgLinkOrUpload, currentgifName, amountOfGifs);
    messageHistoryManager("rm", msg);
    messageHistoryManagerGif("rm", msg);
    deleteMessage(msg);
  }

}

function uploadedGifHandler(msg, currentgifName = []) {
  let keys = new Array();
  let msgAttach = msg.attachments;
  let gifCounter = 0;
  msg.attachments.each(key => {
    keys.push(key.id)
  });

  for (let i = 0; i < msgAttach.size; i++) { //check current msg(which can have multiple gif attachments)
    if (IsMessageAnUploadedGif(msgAttach.get(keys[i]).name)) {
      currentgifName.push(msgAttach.get(keys[i]).name);
      gifCounter++;
    }
  }
  return gifCounter;
}

function linkGifHandler(msg, currentgifName = []) { //bug: if there are 2 links and one of them isn't a gif, but the second is(and is the last thing in the message, then IsMessageALinkGif() will say both are gifs)
  let content = msg.content;
  let contentIterate = content;
  let re = new RegExp(/https:\/\//, 'i');
  let gifCounter = 0;
  let httpLoc = -1; //test this
  while ( (httpLoc = contentIterate.search(re)) !== -1) {
    if (IsMessageALinkGif(contentIterate)) {
      currentgifName.push(contentIterate.substring(httpLoc, contentIterate.search(/.gif/)));
      gifCounter++;
    }
    contentIterate = contentIterate.substring(httpLoc + 1);
  }
  return gifCounter;
}

function IsMessageAnUploadedGif(msgName) {
  if (msgName.substring(msgName.length - 4, msgName.length) === '.gif') {
    return true
  } else {
    false
  }
}

function IsMessageALinkGif(msg) {
  if (msg.substring(msg.length - 4, msg.length) === '.gif'){
    return true
  } 
  else if (msg.substring(0, 18) === 'https://tenor.com/'){
    return true
  }
  else if (msg.substring(0, 29) === 'https://media.discordapp.net/'){
    return true
  } 
  else if (msg.substring(0, 27) === 'https://cdn.discordapp.com/' && msg.includes('gif')){
    return true
  } 
  else return false
}

function messageHistoryManager(op, message = null) {
  if (op === "add") {
    messagesHistory.unshift(message);
  }
  if (op === "rm") {
    messagesHistory.shift();
  }

  if (messagesHistory.length > 10) { //keep the length 10
    messagesHistory.pop() //remove last element
  }

}

function messageHistoryManagerGif(op, message = null) {
  if (op === "add") {
    messagesHistoryGif.unshift(message);
  }
  if (op === "rm") {
    messagesHistoryGif.shift();
  }

  if (messagesHistoryGif.length > 20) { //keep the length 20
    messagesHistoryGif.pop() //remove last element
  }

}

function spamLogger(who, oldMessage, newMessage, longer) {
  for (let i = 0; i < longer.length; i++) {
    if (longer[i] === 0) {
      console.log('|-> \t' + who + ' sent: \"' + newMessage + '\"');
      console.log('| \twhich were equal(or similar) to: ' + '\"' + oldMessage[i] + '\"')
    } else if (longer[i] === 1) {
      console.log('|-> \t' + who + ' sent: \"' + newMessage + '\"');
      console.log('| \twhich was similar(shorter) to: \"' + oldMessage[i] + '\"');
      console.log('| \t' + '(' + ((newMessage.length / oldMessage[i].length) * 100).toFixed(2) + '%)');
    } else if (longer[i] === 2) {
      console.log('|-> \t' + who + ' sent: \"' + newMessage + '\"');
      console.log('| \twhich was similar(longer) to: \"' + oldMessage[i] + '\"');
      console.log('| \t' + '(' + ((oldMessage[i].length / newMessage.length) * 100).toFixed(2) + '%)');
    } else {
      console.log("If you\'re reading this, something fucked up.")
    }
  }
  console.log('|----------------------------------------------------------------');
}

function gifLogger(op, who, msg, msgLinkOrUpload, currentgifName, amountOfGifs) {
  if(op === "inc"){
    if(msgLinkOrUpload === 1){
      console.log('| \t ' + msg.createdAt + ':\n| \t From: ' + who + '\n|-> \t ' + 'New LinkGif!\n|-> \t Name: ');
      currentgifName.forEach(name => {
        console.log('| \t ' + name);
      });
      console.log('|-> \t Amount of gifs: ' + amountOfGifs);
    }
    else if (msgLinkOrUpload === 2){
      console.log('| \t ' + msg.createdAt + ':\n| \t From: ' + who + '\n|-> \t ' + 'New UploadedGif!\n|-> \t Name: ');
      currentgifName.forEach(name => {
        console.log('| \t ' + name);
      });
      console.log('|-> \t Amount of gifs: ' + amountOfGifs);
    }
    else if(msgLinkOrUpload === 3){
      console.log('| \t ' + msg.createdAt + ':\n| \t From: ' + who + '\n|-> \t ' + 'New MixOfGifs!\n|-> \t Name: ');
      currentgifName.forEach(name => {
        console.log('| \t ' + name);
      });
      console.log('|-> \t Amount of gifs: ' + amountOfGifs);
    }
    else{
      console.log("|-> \t' Error in gifLogger. msgLinkOrUpload is not 1|2|3.");
      console.log("| \t Called with: " + op, + "\n" + msg, + "\n" + msgLinkOrUpload, + "\n" + currentgifName, + "\n" + amountOfGifs);
    }
  }

  if(op === "rm"){
    console.log("|-> \t Going to delete: ");
    currentgifName.forEach(name => {
      console.log('|-> \t ' + name);
    });
  }

  console.log('|----------------------------------------------------------------');
}

function deleteMessage(msg) {
  msg.delete()
}

client.login(token)
